# Django-Cypress

## Description
This proof of concept demonstrates using Cypress to test a Django website in a pipeline.

## Overview
1. Uses a Docker image for the runner with the Docker in Docker (dind) service. 
2. Installs Docker Compose on the Docker runner.
3. Uses Docker Compose to `build` the Django app and the Cypress configuration.
4. Uses Docker Compose to `run` the Django app (container name is `web`) and Cypress app (container name is `cypress`). 
5. Cypress confirms that the Django service is available before running the tests. If it is not available, the pipeline will fail.
6. Cypress headlessly executes and tests found in `./cypress/e2e`.
7. Test results are output to the GitLab runner terminal as follows: 
```
$ cat e2e/cypress/results/output.xml
<?xml version="1.0" encoding="UTF-8"?>
<testsuites name="Mocha Tests" time="0.318" tests="1" failures="0">
  <testsuite name="Root Suite" timestamp="2023-07-07T20:09:27" tests="1" file="cypress/e2e/spec.cy.js" time="0.308" failures="0">
    <testcase name="loads page" time="0.245" classname="loads page">
    </testcase>
  </testsuite>
</testsuites>
```
8. Video of the test run is saved as an artifact from the directory `./e2e/cypress/videos/spec.cy.js.mp4`
9. If the Cypress tests fail, the pipeline will fail. 
## Visuals
Sample Output of Cypress Tests from Pipeline

![Runner Output](./runner_output.png)

Sample Artifact Output

![Artifact](spec.cy.js.mp4)

## Configuration Neccessities
1. This project will run as it's configured but if you change the name of the Django container, you must set the `ALLOWED_HOSTS` variable in the Django settings file to allow the name of your Django container. In this case, `web` is the name of the container and was therefore added to the `ALLOWED_HOSTS`. For example, ensure `your_container_name` is included in the `ALLOWED_HOSTS` in the file `./app/example/setting.py`
```
ALLOWED_HOSTS = env.list("ALLOWED_HOSTS", ("localhost", "127.0.0.1", "your_container_name"))
```

## Usage
If you would like to see the Cypress test fail, change "worked" in `./e2e/cypress/e2e/spec.cy.js` to something like "workd" and run the pipeline. 

## Authors and Acknowledgment
Thanks to [Martin Bartolo](https://www.martinbartolo.com) for finding this helpful [link](https://github.com/cypress-io/cypress-example-docker-compose) for the Cypress example. 

## License
No license. Feel free to copy and use. 

## Project Status
No further development. 
